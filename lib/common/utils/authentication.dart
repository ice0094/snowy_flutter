import 'dart:async';

import 'package:flutter/material.dart';
import 'package:snowy_flutter/common/routes/app_pages.dart';
import 'package:snowy_flutter/common/utils/utils.dart';
import 'package:snowy_flutter/common/values/values.dart';
import 'package:get/get.dart';

import '../../global.dart';
/// 检查是否有 token
Future<bool> isAuthenticated() async {
  var profileJSON = StorageUtil().getJSON(STORAGE_USER_PROFILE_KEY);
  return profileJSON != null ? true : false;
}

/// 删除缓存 token
Future deleteAuthentication() async {
  await StorageUtil().remove(STORAGE_TOKEN_KEY);
  await StorageUtil().remove(STORAGE_USER_PROFILE_KEY);
  // Global.profile = UserLoginResponseEntity(
  //   accessToken: null,
  // );
}

/// 重新登录
Future goLoginPage() async {
  await deleteAuthentication();
  Get.offAndToNamed(AppRoutes.SIGN_IN);
}
