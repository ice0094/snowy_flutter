import 'dart:async';
import 'package:dio/dio.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:snowy_flutter/common/utils/utils.dart';
import 'package:snowy_flutter/common/values/values.dart';
import 'package:snowy_flutter/global.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import 'capture.dart';
import 'http_error.dart';
import 'net_cache.dart';
import 'interceptors_wrapper.dart';
import 'options.dart';

/*
  * http 操作类
  *
  * 手册
  * https://github.com/flutterchina/dio/blob/master/README-ZH.md#formdata
  *
  * 从2.1.x升级到 3.x
  * https://github.com/flutterchina/dio/blob/master/migration_to_3.0.md
*/
class HttpUtil {
  static HttpUtil _instance = HttpUtil._internal();
  factory HttpUtil() => _instance;

  Dio dio = new Dio();
  CancelToken cancelToken = new CancelToken();

  HttpUtil._internal() {
    // dio基础选项配置
    dio.options = baseOptions;
    // Cookie管理
    dio.interceptors.add(CookieManager(CookieJar()));
    // 添加拦截器
    dio.interceptors.add(interceptorsWrapper);
    // 缓存
    dio.interceptors.add(NetCache());
    // 在调试模式下需要抓包调试，所以我们使用代理，并禁用HTTPS证书校验
    capture(dio);
  }



  /*
   * 取消请求
   *
   * 同一个cancel token 可以用于多个请求，当一个cancel token取消时，所有使用该cancel token的请求都会被取消。
   * 所以参数可选
   */
  void cancelRequests(CancelToken token) {
    token.cancel("cancelled");
  }


  /// 读取本地配置
  Map<String, dynamic>? getAuthorizationHeader(String path) {
    Map<String, dynamic>? headers;
    String? accessToken = Global.token;
    if (accessToken != null && path != "/login") {
      headers = {
        'Authorization': 'Bearer $accessToken',
      };
    }
    return headers;
  }

  /// restful get 操作
  /// restful get 操作
  /// refresh 是否下拉刷新 默认 false
  /// noCache 是否不缓存 默认 true
  /// list 是否列表 默认 false
  /// cacheKey 缓存key
  Future get(String path, {
    dynamic params,
    Options? options,
    bool refresh = false,
    bool noCache = !CACHE_ENABLE,
    bool list = false,
    String? cacheKey,
    CancelToken? cancelToken,
    bool cacheDisk = false,
}) async {
    EasyLoading.show(status: '请求中...');
    Options requestOptions = options ?? Options();
    requestOptions.extra = {
      "refresh": refresh,
      "noCache": noCache,
      "list": list,
      "cacheKey": cacheKey,
      "cacheDisk": cacheDisk,
    };
    Map<String, dynamic>? _authorization = getAuthorizationHeader(path);
    if (_authorization != null) {
      requestOptions.headers = _authorization;
    }
    try {
      var response = await dio.get(path, queryParameters: params, options: requestOptions, cancelToken: cancelToken);
      EasyLoading.dismiss();
      return response.data;
    } on DioError catch (e) {
      EasyLoading.dismiss();
      throw createHttpError(e);
    }
  }

  /// restful post 操作
  Future post(String path, {dynamic params, Options? options, CancelToken? cancelToken}) async {
    EasyLoading.show(status: '请求中...');
    Options requestOptions = options ?? Options();
    Map<String, dynamic>? _authorization = getAuthorizationHeader(path);
    if (_authorization != null) {
      requestOptions.headers = _authorization;
    }
    try{
      var response = await dio.post(path, data: params, options: requestOptions, cancelToken: cancelToken);
      EasyLoading.dismiss();
      return response.data;
    } on DioError catch (e) {
      EasyLoading.dismiss();
      throw createHttpError(e);
    }
  }

  /// restful put 操作
  Future put(String path, {dynamic params, Options? options, CancelToken? cancelToken}) async {
    EasyLoading.show(status: '请求中...');
    Options requestOptions = options ?? Options();
    Map<String, dynamic>? _authorization = getAuthorizationHeader(path);
    if (_authorization != null) {
      requestOptions.headers = _authorization;
    }
    try{
      var response = await dio.put(path, data: params, options: requestOptions, cancelToken: cancelToken);
      EasyLoading.dismiss();
      return response.data;
    } on DioError catch (e) {
      EasyLoading.dismiss();
      throw createHttpError(e);
    }
  }

  /// restful delete 操作
  Future delete(String path, {dynamic params, Options? options, CancelToken? cancelToken}) async {
    EasyLoading.show(status: '请求中...');
    Options requestOptions = options ?? Options();
    Map<String, dynamic>? _authorization = getAuthorizationHeader(path);
    if (_authorization != null) {
      requestOptions.headers = _authorization;
    }
    try{
      var response = await dio.delete(path, data: params, options: requestOptions, cancelToken: cancelToken);
      EasyLoading.dismiss();
      return response.data;
    } on DioError catch (e) {
      EasyLoading.dismiss();
      throw createHttpError(e);
    }
  }

  /// restful post form 表单提交操作
  Future postForm(String path, {dynamic params, Options? options, CancelToken? cancelToken}) async {
    EasyLoading.show(status: '请求中...');
    Options requestOptions = options ?? Options();
    Map<String, dynamic>? _authorization = getAuthorizationHeader(path);
    if (_authorization != null) {
      requestOptions.headers = _authorization;
    }
    try{
      var response = await dio.post(path, data: FormData.fromMap(params), options: requestOptions, cancelToken: cancelToken);
      EasyLoading.dismiss();
      return response.data;
    } on DioError catch (e) {
      EasyLoading.dismiss();
      throw createHttpError(e);
    }
  }
}

