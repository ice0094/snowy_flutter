
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:snowy_flutter/common/routes/app_pages.dart';
import 'package:get/get.dart' as getX;
import 'package:dio/dio.dart';

import '../authentication.dart';

InterceptorsWrapper interceptorsWrapper = InterceptorsWrapper(
    onRequest: (RequestOptions options, RequestInterceptorHandler interceptorHandler) {
      // debugPrint("请求之前");
      // Loading.before(options.uri, '正在通讯...');
      return interceptorHandler.next(options); //continue
    },
    onResponse: (Response response, ResponseInterceptorHandler interceptorHandler) {
      // debugPrint("响应之前");
      var code = response.data['code'];
      if(code == 1011006 || code == 1011007 || code == 1011008 || code == 1011009){
        // token失效或异常
        Future.delayed(Duration(seconds: 1), () => getX.Get.snackbar(
            "提示",
            response.data['message'],
            colorText: Colors.white,
            backgroundColor:Colors.orange,
        ));
        goLoginPage();
      }else if(code == 1013002 || code == 1016002 || code == 1015002){
        // 无权限
        Future.delayed(Duration(seconds: 1), () => getX.Get.snackbar(
          "提示",
          response.data['message'],
          colorText: Colors.white,
          backgroundColor:Colors.orange,
        ));
      }

      // Loading.complete(response.request.uri);
      return interceptorHandler.next(response); // continue
    },
    onError: (DioError e,  ErrorInterceptorHandler interceptorHandler) {
      // debugPrint("错误之前");
      // Loading.complete(e.request.uri);
      return interceptorHandler.next(e); //continue
    }
);