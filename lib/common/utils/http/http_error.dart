import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HttpError implements Exception {
  int code;
  String? message;
  HttpError({required this.code, required this.message});

  String toString() {
    if (message == null) return "Exception";
    return "Exception: code $code, $message";
  }
}

// 错误信息
HttpError createHttpError(DioError error) {
  switch (error.type) {
    case DioErrorType.cancel:{
      return HttpError(code: -1, message: "请求取消");
    }
    case DioErrorType.connectTimeout:{
      return HttpError(code: -1, message: "连接超时");
    }
    case DioErrorType.sendTimeout:{
      return HttpError(code: -1, message: "请求超时");
    }
    case DioErrorType.receiveTimeout:{
      return HttpError(code: -1, message: "响应超时");
    }
    case DioErrorType.response:{
      Get.snackbar(
          "提示",
          "后端未启动",
          colorText: Colors.white,
          backgroundColor:Colors.pink
      );
      return HttpError(code: -1, message: "后端未启动");
      try {
        int errCode = error.response!.statusCode!;
        // String errMsg = error.response.statusMessage;
        // return ErrorEntity(code: errCode, message: errMsg);
        switch (errCode) {
          case 400:{
            return HttpError(code: errCode, message: "请求语法错误");
          }
          case 401:{
            return HttpError(code: errCode, message: "没有权限");
          }
          case 403:{
            return HttpError(code: errCode, message: "服务器拒绝执行");
          }
          case 404:{
            return HttpError(code: errCode, message: "无法连接服务器");
          }
          case 405:{
            return HttpError(code: errCode, message: "请求方法被禁止");
          }
          case 500:{
            return HttpError(code: errCode, message: "服务器内部错误");
          }
          case 502:{
            return HttpError(code: errCode, message: "无效的请求");
          }
          case 503:{
            return HttpError(code: errCode, message: "服务器挂了");
          }
          case 505:{
            return HttpError(code: errCode, message: "不支持HTTP协议请求");
          }
          default:{

            // return ErrorEntity(code: errCode, message: "未知错误");
            return HttpError(code: errCode, message: error.response?.statusMessage!);
          }
        }
      } on Exception catch (_) {
        // Future.delayed(Duration(seconds: 1), () => Get.snackbar("提示", "后端未启动"));
        return HttpError(code: -1, message: "未知错误");
      }
    }
    default:{
        return HttpError(code: -1, message: error.message);
    }
  }
}