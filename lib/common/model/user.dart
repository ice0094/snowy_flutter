import 'package:flutter/foundation.dart';
// 用户信息
class User {
  // String? accessToken;
  String? name;
  String? avatar;
  DateTime? birthday;
  int? sex;
  String? phone;

  User({
    // this.accessToken,
    this.name,
    this.avatar,
    this.birthday,
    this.sex,
    this.phone,
  });

  factory User.fromJson(Map<String, dynamic> json) =>
      User(
        // accessToken: json["access_token"],
        name: json["name"],
        avatar: json["avatar"],
        birthday: json["birthday"],
        sex: json["sex"],
        phone: json["phone"],
      );
  Map<String, dynamic> toJson() => {
    // "access_token": accessToken,
    "name": name,
    "avatar": avatar,
    "birthday": birthday,
    "sex": sex,
    "phone": phone,

  };
}
