import 'package:flutter/material.dart';
import 'package:snowy_flutter/common/widgets/captcha/verify/block_puzzle_captcha.dart';
import 'package:snowy_flutter/common/widgets/captcha/verify/click_word_captcha.dart';


//点选拼图
void loadingClickWord(BuildContext context, {
  barrierDismissible = true,
  dynamic Function(String v)? onSuccess,
  dynamic Function()? onFail,
}) {
  showDialog<Null>(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (BuildContext context) {
      return ClickWordCaptcha(
        onSuccess: onSuccess,
        onFail: onFail,
      );
    },
  );
}

//滑动拼图
void loadingBlockPuzzle(BuildContext context, {barrierDismissible = true}) {
  showDialog<Null>(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (BuildContext context) {
      return BlockPuzzleCaptcha(
        onSuccess: (v){

        },
        onFail: (){

        },
      );
    },
  );
}