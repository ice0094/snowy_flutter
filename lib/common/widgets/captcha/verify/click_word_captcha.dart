import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:snowy_flutter/common/utils/utils.dart';
import 'package:snowy_flutter/common/widgets/captcha/model/click_word_captcha_model.dart';
import 'package:snowy_flutter/common/widgets/captcha/utils/object_utils.dart';
import 'package:snowy_flutter/common/widgets/captcha/utils/widget_util.dart';
import 'package:steel_crypt/steel_crypt.dart';

import '../enum.dart';
import '../utils/encrypt_util.dart';

typedef VoidSuccessCallback = dynamic Function(String v);

class ClickWordCaptcha extends StatefulWidget {
  final VoidSuccessCallback? onSuccess; //文字点击后验证成功回调
  final VoidCallback? onFail; //文字点击完成后验证失败回调

  const ClickWordCaptcha({Key? key, this.onSuccess, this.onFail}) : super(key: key);

  @override
  _ClickWordCaptchaState createState() => _ClickWordCaptchaState();
}

class _ClickWordCaptchaState extends State<ClickWordCaptcha> {
  ClickWordVerifyState _clickWordVerifyState = ClickWordVerifyState.none;
  List<Offset> _tapOffsetList = [];
  ClickWordCaptchaModel _clickWordCaptchaModel = ClickWordCaptchaModel();

  Color titleColor = Colors.black;
  Color borderColor = Color(0xffdddddd);
  String bottomTitle = "";
  Size baseSize = Size(310.0, 155.0);

  //改变底部样式及字段
  _changeResultState() {
    switch (_clickWordVerifyState) {
      case ClickWordVerifyState.normal:
        titleColor = Colors.black;
        borderColor = Color(0xffdddddd);
        break;
      case ClickWordVerifyState.success:
        _tapOffsetList = [];
        titleColor = Colors.green;
        borderColor = Colors.green;
        bottomTitle = "验证成功";
        break;
      case ClickWordVerifyState.fail:
        _tapOffsetList = [];
        titleColor = Colors.red;
        borderColor = Colors.red;
        bottomTitle = "验证失败";
        break;
      default:
        titleColor = Colors.black;
        borderColor = Color(0xffdddddd);
        bottomTitle = "数据加载中……";
        break;
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _loadCaptcha();
  }

  //加载验证码
  _loadCaptcha() async {
    _tapOffsetList = [];
    _clickWordVerifyState = ClickWordVerifyState.none;
    _changeResultState();
    var res = await HttpUtil().post("/captcha/get",params: {"captchaType": "clickWord"});
    if (res['repCode'] != '0000' || res['repData'] == null) {
      _clickWordCaptchaModel.secretKey = "";
      bottomTitle = "加载失败,请刷新";
      _clickWordVerifyState = ClickWordVerifyState.normal;
      _changeResultState();
      return;
    } else {
      Map<String, dynamic> repData = res['repData'];
      _clickWordCaptchaModel = ClickWordCaptchaModel.fromMap(repData);
      var baseR = await WidgetUtil.getImageWH(image: Image.memory(Base64Decoder().convert(_clickWordCaptchaModel.imgStr)));
      baseSize = baseR.size;
      bottomTitle = "请依次点击【${_clickWordCaptchaModel.wordStr}】";
    }
    _clickWordVerifyState = ClickWordVerifyState.normal;
    _changeResultState();
  }

  //校验验证码
  _checkCaptcha() async {
    List<Map<String, dynamic>> mousePos = [];
    _tapOffsetList.map((size) {
      mousePos.add({"x": size.dx.roundToDouble(), "y": size.dy.roundToDouble()});
    }).toList();
    var pointStr = json.encode(mousePos);

    print(pointStr);
    print(mousePos.toString());

    var cryptedStr = pointStr;

    // secretKey 不为空 进行as加密
    if(!ObjectUtils.isEmpty(_clickWordCaptchaModel.secretKey)){

      /// String-->List<int>-->base64

      var aesCrypt = AesCrypt(key: base64Encode(utf8.encode(_clickWordCaptchaModel.secretKey)), padding: PaddingAES.pkcs7);
      cryptedStr = aesCrypt.ecb.encrypt(inp: pointStr);
      var dcrypt = aesCrypt.ecb.decrypt(enc: cryptedStr);
    }

    // 发送后端请求
    // var captchaVerification = !ObjectUtils.isEmpty(_clickWordCaptchaModel.secretKey)
    //     ? EncryptUtil.aesEncode(key: _clickWordCaptchaModel.secretKey, content: _clickWordCaptchaModel.token + '---' + pointStr)
    //     : _clickWordCaptchaModel.token + '---' + pointStr;

//    Map _map = json.decode(dcrypt);
    var res = await HttpUtil().post("/captcha/check",params: {
      "pointJson": cryptedStr,
      "captchaType": "clickWord",
      "token": _clickWordCaptchaModel.token
    });
    if (res['repCode'] != '0000' || res['repData'] == null) {
      _checkFail();
      return;
    }
    Map<String, dynamic> repData = res['repData'];
    if (repData["result"] != null && repData["result"] == true) {
      //如果不加密  将  token  和 坐标序列化 通过  --- 链接成字符串
      var captchaVerification = "${_clickWordCaptchaModel.token}---$pointStr";

      if(!ObjectUtils.isEmpty(_clickWordCaptchaModel.secretKey)){
        //如果加密  将  token  和 坐标序列化 通过  --- 链接成字符串 进行加密  加密密钥为 _clickWordCaptchaModel.secretKey
        captchaVerification = EncryptUtil.aesEncode(key: _clickWordCaptchaModel.secretKey, content: captchaVerification);
      }

      _checkSuccess(captchaVerification);
    } else {
      _checkFail();
    }
  }

  //校验失败
  _checkFail() async {
    _clickWordVerifyState = ClickWordVerifyState.fail;
    _changeResultState();
    await Future.delayed(Duration(milliseconds: 1000));
    _loadCaptcha();
    //回调
    if (widget.onFail != null) {
      widget.onFail!();
    }
  }

  //校验成功
  _checkSuccess(String pointJson) async {
    _clickWordVerifyState = ClickWordVerifyState.success;
    _changeResultState();
    await Future.delayed(Duration(milliseconds: 1000));
    // var aesCrypt = AesCrypt(padding: PaddingAES.pkcs7, key: base64Encode(utf8.encode('BGxdEUOZkXka4HSj')));
    // var cryptedStr = aesCrypt.ecb.encrypt(inp: pointJson);
    // print(cryptedStr);
    //回调   pointJson 是经过es加密之后的信息
    if (widget.onSuccess != null) {
      widget.onSuccess!(pointJson);
    }
    //关闭
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    var data = MediaQuery.of(context);
    var dialogWidth = 0.9 * data.size.width;
    var isRatioCross = false;
    if(dialogWidth < 320.0 ){
      dialogWidth = data.size.width;
      isRatioCross = true;
    }
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Container(
          width: dialogWidth,
          height: 320,
          color: Colors.white,
          child: Column(
            children: <Widget>[
              _topContainer(),
              _captchaContainer(),
              _bottomContainer()
            ],
          ),
        ),
      ),
    );
  }

  //图片验证码
  _captchaContainer() {
    List<Widget> _widgetList = [];
    double _widgetW = 20;
    for (int i = 0; i < _tapOffsetList.length; i++) {
      Offset offset = _tapOffsetList[i];
      _widgetList.add(Positioned(
          left: offset.dx - _widgetW * 0.5,
          top: offset.dy - _widgetW * 0.5,
          child: Container(
            alignment: Alignment.center,
            width: _widgetW,
            height: _widgetW,
            decoration: BoxDecoration(color: Color(0xCC43A047), borderRadius: BorderRadius.all(Radius.circular(_widgetW))),
            child: Text(
              "${i + 1}",
              style: TextStyle(color: Colors.white, fontSize: 15),
            ),
          )));
    }
    _widgetList.add(//刷新按钮
        Positioned(top: 0, right: 0, child: IconButton(
          icon: Icon(Icons.refresh),
          iconSize: 30,
          color: Colors.deepOrangeAccent,
          onPressed: () {
            //刷新
            _loadCaptcha();
          }),
    ));

    return GestureDetector(
        onTapDown: (TapDownDetails details) {
          debugPrint("onTapDown globalPosition全局坐标系位置:  ${details.globalPosition} localPosition组件坐标系位置: ${details.localPosition} ");
          if (!ObjectUtils.isListEmpty(_clickWordCaptchaModel.wordList) && _tapOffsetList.length < _clickWordCaptchaModel.wordList.length) {
            _tapOffsetList.add(Offset(details.localPosition.dx, details.localPosition.dy));
          }
          setState(() {

          });
          if (!ObjectUtils.isListEmpty(_clickWordCaptchaModel.wordList) && _tapOffsetList.length == _clickWordCaptchaModel.wordList.length) {
            _checkCaptcha();
          }
        },
        child: Container(
          width: baseSize.width,
          height: baseSize.height,
          decoration: BoxDecoration(
              image: ObjectUtils.isEmpty(_clickWordCaptchaModel.imgStr)
                  ? null
                  : DecorationImage(image: MemoryImage(Base64Decoder().convert(_clickWordCaptchaModel.imgStr)))),
          child: Stack(
            children: _widgetList,
          ),
        ));
  }

  //底部提示部件
  _bottomContainer() {
    return Container(
      height: 50,
      margin: EdgeInsets.only(top: 10),
      alignment: Alignment.center,
      width: baseSize.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(4)),
          border: Border.all(color: borderColor)),
      child:
          Text(bottomTitle, style: TextStyle(fontSize: 18, color: titleColor)),
    );
  }

  //顶部，提示+关闭
  _topContainer() {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      margin: EdgeInsets.only(bottom: 20, top: 5),
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(width: 1, color: Color(0xffe5e5e5))),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            '请完成安全验证',
            style: TextStyle(fontSize: 18),
          ),
          IconButton(
              icon: Icon(Icons.highlight_off),
              iconSize: 35,
              color: Colors.black54,
              onPressed: () {
                //退出
                Navigator.pop(context);
              }),
        ],
      ),
    );
  }
}
