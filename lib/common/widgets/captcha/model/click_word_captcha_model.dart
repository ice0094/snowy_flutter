//请求数据模型
import 'dart:convert';

class ClickWordCaptchaModel {
  String imgStr; //图表url 目前用base64 data
  String token; // 获取的token 用于校验
  List wordList; //显示需要点选的字
  String wordStr; //显示需要点选的字转换为字符串
  String secretKey; //加密key

  ClickWordCaptchaModel({
    this.imgStr = "",
    this.token = "",
    this.secretKey = "",
    this.wordList = const [],
    this.wordStr = ""
  });

  //解析数据转换模型
  static ClickWordCaptchaModel fromMap(Map<String, dynamic> map) {
    ClickWordCaptchaModel captchaModel = ClickWordCaptchaModel();
    captchaModel.imgStr = map["originalImageBase64"] ?? "";
    captchaModel.token = map["token"] ?? "";
    captchaModel.secretKey = map["secretKey"] ?? "";
    captchaModel.wordList = map["wordList"] ?? [];

    if (captchaModel.wordList.isNotEmpty) {
      captchaModel.wordStr = captchaModel.wordList.join(",");
    }

    return captchaModel;
  }

  //将模型转换
  Map<String, dynamic> toJson() {
    var map = new Map<String, dynamic>();
    map['imgStr'] = imgStr;
    map['token'] = token;
    map['secretKey'] = token;
    map['wordList'] = wordList;
    map['wordStr'] = wordStr;
    return map;
  }

  @override
  String toString() {
    // TODO: implement toString
    return JsonEncoder.withIndent('  ').convert(toJson());
  }
}