import 'package:flutter/material.dart';
import 'package:snowy_flutter/common/apis/apis.dart';
import 'package:snowy_flutter/common/routes/app_pages.dart';
import 'package:snowy_flutter/common/utils/storage.dart';
import 'package:snowy_flutter/common/values/storage.dart';
import 'package:snowy_flutter/global.dart';

import 'package:get/get.dart';

/// 检查是否登录
class RouteAuthMiddleware extends GetMiddleware {
  // priority 数字小优先级高
  @override
  int? priority = 0;

  RouteAuthMiddleware({required this.priority});

  @override
  RouteSettings? redirect(String? route) {
    if (Global.token != null || route == AppRoutes.SIGN_IN || route == AppRoutes.SIGN_UP || route == AppRoutes.INITIAL) {
      return null;
    } else {
      Future.delayed(Duration(seconds: 1), () => Get.snackbar(
          "提示",
          "AccessToken异常,请重新登录",
          colorText: Colors.white,
          backgroundColor:Colors.pink
      ));
      return RouteSettings(name: AppRoutes.SIGN_IN);
    }
  }



}
