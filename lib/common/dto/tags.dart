/// 标签列表 Request
class TagRequest {
  String? categoryCode;
  String? channelCode;
  String? tag;
  String? keyword;
  String? newsID;

  TagRequest({
    this.categoryCode,
    this.channelCode,
    this.tag,
    this.keyword,
    this.newsID,
  });

  Map<String, dynamic> toJson() => {
    "categoryCode": categoryCode,
    "channelCode": channelCode,
    "tag": tag,
    "keyword": keyword,
    "newsID": newsID,
  };
}

/// 标签列表 Response
class TagResponse {
  String? tag;

  TagResponse({
    this.tag,
  });

  factory TagResponse.fromJson(Map<String, dynamic> json) =>
      TagResponse(
        tag: json["tag"],
      );

  Map<String, dynamic> toJson() => {
    "tag": tag,
  };
}
