import 'dart:convert';

// 验证码开启返回
CaptchaOpenResponse captchaOpenResponseFromJson(String str) => CaptchaOpenResponse.fromJson(json.decode(str));

String captchaOpenResponseToJson(CaptchaOpenResponse data) => json.encode(data.toJson());

class CaptchaOpenResponse {

  bool success;
  int? code;
  String? message;
  bool data;

  CaptchaOpenResponse({
    required this.success,
    this.code,
    this.message,
    required this.data,
  });

  factory CaptchaOpenResponse.fromJson(Map<String, dynamic> json) => CaptchaOpenResponse(
    success: json["success"],
    code: json["code"],
    message: json["message"],
    data: json["data"],
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "code": code,
    "message": message,
    "data": data,
  };
}


// 登录请求
class LoginRequest {
  String account;
  String password;
  String? code;

  LoginRequest({
    required this.account,
    required this.password,
    this.code,
  });

  factory LoginRequest.fromJson(Map<String, dynamic> json) =>
      LoginRequest(
        account: json["account"],
        password: json["password"],
        code: json["code"],
      );

  Map<String, dynamic> toJson() => {
    "account": account,
    "password": password,
    "code": code,
  };
}

// 登录返回
class LoginResponse {
  int code;
  String data;
  String message;
  bool success;

  LoginResponse({
    required this.code,
    required this.data,
    required this.message,
    required this.success
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      LoginResponse(
        code: json["code"],
        data: json["data"] ?? "",
        message: json["message"],
        success: json["success"],
      );

  Map<String, dynamic> toJson() => {
    "code" : code,
    "data" : data,
    "message" : message,
    "success" : success
  };
}

// 获取登录用户信息返回
LoginUserResponse loginUserResponseFromJson(String str) => LoginUserResponse.fromJson(json.decode(str));

String loginUserResponseToJson(LoginUserResponse data) => json.encode(data.toJson());

class LoginUserResponse {

  bool success;
  int code;
  String message;
  Data? data;

  LoginUserResponse({
    required this.success,
    required this.code,
    required this.message,
    this.data,
  });



  factory LoginUserResponse.fromJson(Map<String, dynamic> json) => LoginUserResponse(
    success: json["success"],
    code: json["code"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "success": success,
    "code": code,
    "message": message,
    "data": data?.toJson(),
  };
}

class Data {

  String? id;
  String? account;
  String? nickName;
  String? name;
  String? avatar;
  DateTime? birthday;
  int? sex;
  String? email;
  String? phone;
  String? tel;
  int? adminType;
  String? lastLoginIp;
  DateTime? lastLoginTime;
  String? lastLoginAddress;
  String? lastLoginBrowser;
  String? lastLoginOs;
  LoginEmpInfo? loginEmpInfo;
  List<App>? apps;
  List<dynamic>? roles;
  List<dynamic>? permissions;
  List<Menu>? menus;
  List<dynamic>? dataScopes;
  dynamic tenants;
  List<dynamic>? authorities;
  bool? enabled;
  dynamic password;
  String? username;
  bool? accountNonLocked;
  bool? accountNonExpired;
  bool? credentialsNonExpired;

  Data({
    this.id,
    this.account,
    this.nickName,
    this.name,
    this.avatar,
    this.birthday,
    this.sex,
    this.email,
    this.phone,
    this.tel,
    this.adminType,
    this.lastLoginIp,
    this.lastLoginTime,
    this.lastLoginAddress,
    this.lastLoginBrowser,
    this.lastLoginOs,
    this.loginEmpInfo,
    this.apps,
    this.roles,
    this.permissions,
    this.menus,
    this.dataScopes,
    this.tenants,
    this.authorities,
    this.enabled,
    this.password,
    this.username,
    this.accountNonLocked,
    this.accountNonExpired,
    this.credentialsNonExpired,
  });



  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    account: json["account"],
    nickName: json["nickName"],
    name: json["name"],
    avatar: json["avatar"],
    birthday: json["birthday"]== null ? null : DateTime.parse(json["birthday"]),
    sex: json["sex"],
    email: json["email"],
    phone: json["phone"],
    tel: json["tel"],
    adminType: json["adminType"],
    lastLoginIp: json["lastLoginIp"],
    lastLoginTime: DateTime.parse(json["lastLoginTime"]),
    lastLoginAddress: json["lastLoginAddress"],
    lastLoginBrowser: json["lastLoginBrowser"],
    lastLoginOs: json["lastLoginOs"],
    loginEmpInfo: LoginEmpInfo.fromJson(json["loginEmpInfo"]),
    apps: List<App>.from(json["apps"].map((x) => App.fromJson(x))),
    roles: List<dynamic>.from(json["roles"].map((x) => x)),
    permissions: List<dynamic>.from(json["permissions"].map((x) => x)),
    menus: List<Menu>.from(json["menus"].map((x) => Menu.fromJson(x))),
    dataScopes: List<dynamic>.from(json["dataScopes"].map((x) => x)),
    tenants: json["tenants"],
    authorities: List<dynamic>.from(json["authorities"].map((x) => x)),
    enabled: json["enabled"],
    password: json["password"],
    username: json["username"],
    accountNonLocked: json["accountNonLocked"],
    accountNonExpired: json["accountNonExpired"],
    credentialsNonExpired: json["credentialsNonExpired"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "account": account,
    "nickName": nickName,
    "name": name,
    "avatar": avatar,
    "birthday": birthday?.toIso8601String(),
    "sex": sex,
    "email": email,
    "phone": phone,
    "tel": tel,
    "adminType": adminType,
    "lastLoginIp": lastLoginIp,
    "lastLoginTime": lastLoginTime?.toIso8601String(),
    "lastLoginAddress": lastLoginAddress,
    "lastLoginBrowser": lastLoginBrowser,
    "lastLoginOs": lastLoginOs,
    "loginEmpInfo": loginEmpInfo?.toJson(),
    "apps": List<dynamic>.from(apps?.map((x) => x.toJson())??[]),
    "roles": List<dynamic>.from(roles?.map((x) => x)??[]),
    "permissions": List<dynamic>.from(permissions?.map((x) => x)??[]),
    "menus": List<dynamic>.from(menus?.map((x) => x.toJson())??[]),
    "dataScopes": List<dynamic>.from(dataScopes?.map((x) => x)??[]),
    "tenants": tenants,
    "authorities": List<dynamic>.from(authorities?.map((x) => x)??[]),
    "enabled": enabled,
    "password": password,
    "username": username,
    "accountNonLocked": accountNonLocked,
    "accountNonExpired": accountNonExpired,
    "credentialsNonExpired": credentialsNonExpired,
  };
}

class App {

  String? code;
  String? name;
  bool? active;

  App({
    this.code,
    this.name,
    this.active,
  });



  factory App.fromJson(Map<String, dynamic> json) => App(
    code: json["code"],
    name: json["name"],
    active: json["active"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "name": name,
    "active": active,
  };
}

class LoginEmpInfo {

  dynamic jobNum;
  dynamic orgId;
  dynamic orgName;
  List<dynamic>? extOrgPos;
  List<dynamic>? positions;

  LoginEmpInfo({
    this.jobNum,
    this.orgId,
    this.orgName,
    this.extOrgPos,
    this.positions,
  });



  factory LoginEmpInfo.fromJson(Map<String, dynamic> json) => LoginEmpInfo(
    jobNum: json["jobNum"],
    orgId: json["orgId"],
    orgName: json["orgName"],
    extOrgPos: List<dynamic>.from(json["extOrgPos"].map((x) => x)),
    positions: List<dynamic>.from(json["positions"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "jobNum": jobNum,
    "orgId": orgId,
    "orgName": orgName,
    "extOrgPos": List<dynamic>.from(extOrgPos?.map((x) => x)??[]),
    "positions": List<dynamic>.from(positions?.map((x) => x)??[]),
  };
}

class Menu {

  String? id;
  String? pid;
  String? name;
  String? component;
  String? redirect;
  Meta? meta;
  String? path;
  bool? hidden;

  Menu({
    this.id,
    this.pid,
    this.name,
    this.component,
    this.redirect,
    this.meta,
    this.path,
    this.hidden,
  });



  factory Menu.fromJson(Map<String, dynamic> json) => Menu(
    id: json["id"],
    pid: json["pid"],
    name: json["name"],
    component: json["component"],
    redirect: json["redirect"] == null ? null : json["redirect"],
    meta: Meta.fromJson(json["meta"]),
    path: json["path"],
    hidden: json["hidden"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "pid": pid,
    "name": name,
    "component": component,
    "redirect": redirect == null ? null : redirect,
    "meta": meta?.toJson(),
    "path": path,
    "hidden": hidden,
  };
}

class Meta {
  Meta({
    this.title,
    this.icon,
    this.show,
    this.target,
    this.link,
  });

  String? title;
  String? icon;
  bool? show;
  dynamic target;
  dynamic link;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    title: json["title"],
    icon: json["icon"] == null ? null : json["icon"],
    show: json["show"],
    target: json["target"],
    link: json["link"],
  );

  Map<String, dynamic> toJson() => {
    "title": title,
    "icon": icon == null ? null : icon,
    "show": show,
    "target": target,
    "link": link,
  };
}
