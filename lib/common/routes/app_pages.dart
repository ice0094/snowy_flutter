import 'package:snowy_flutter/common/middlewares/router_auth.dart';
import 'package:snowy_flutter/common/middlewares/router_welcome.dart';
import 'package:snowy_flutter/pages/application/view.dart';
import 'package:snowy_flutter/pages/frame/sign_in/view.dart';
import 'package:snowy_flutter/pages/frame/sign_up/view.dart';
import 'package:snowy_flutter/pages/frame/welcome/view.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';


part 'app_routes.dart';

class AppPages {

  static const INITIAL = AppRoutes.INITIAL;

  static final List<GetPage> routes = [
    // 免登陆
    GetPage(
      name: AppRoutes.INITIAL,
      page: () => WelcomePage(),
      // binding: WelcomeBinding(),
      middlewares: [
        RouteWelcomeMiddleware(priority: 1),
      ],
    ),
    GetPage(
      name: AppRoutes.SIGN_IN,
      page: () => SignInPage(),
    ),
    GetPage(
      name: AppRoutes.SIGN_UP,
      page: () => SignUpPage(),
    ),

    // 需要登录
    GetPage(
      name: AppRoutes.Application,
      page: () => ApplicationPage(),
      middlewares: [
        RouteAuthMiddleware(priority: 1),
      ],
    ),
  ];
}