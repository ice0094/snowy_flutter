import 'dart:developer';

import 'package:snowy_flutter/common/dto/dto.dart';
import 'package:snowy_flutter/common/utils/utils.dart';

/// 用户
class UserAPI {

  /// 验证码是否开启
  static Future<CaptchaOpenResponse> getCaptchaOpen() async {
    var response = await HttpUtil().get('/getCaptchaOpen');
    return CaptchaOpenResponse.fromJson(response);
  }

  /// 登录
  static Future<LoginResponse> login({LoginRequest? params}) async {
    var response = await HttpUtil().post('/login', params: params);
    return LoginResponse.fromJson(response);
  }

  /// 获取当前登录用户信息
  static Future<LoginUserResponse> getLoginUser() async {
    var response = await HttpUtil().get('/getLoginUser');
    return LoginUserResponse.fromJson(response);
  }

}