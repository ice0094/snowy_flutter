import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:snowy_flutter/common/widgets/widgets.dart';
import 'package:snowy_flutter/common/utils/screen.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import 'logic.dart';
import 'state.dart';

class MainPage extends StatelessWidget {

  final MainLogic logic = Get.put(MainLogic());
  final MainState state = Get.find<MainLogic>().state;
  @override
  Widget build(BuildContext context) {
    _buildBackgroundImage(){
      return AspectRatio(
        aspectRatio: 1.2,
        child: Image.asset('assets/images/main-bg.png', fit: BoxFit.fill,),
      );
    }

    _buildCategoryApp(){
      return Positioned(
          top: (MediaQuery.of(context).size.width / 1.2) - 24.0,
          bottom: 0,
          left: 0,
          right: 0,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(32.0),
                topRight: Radius.circular(32.0),
              ),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    offset: const Offset(1.1, 1.1),
                    blurRadius: 10.0
                ),
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 10, right: 10,top: 24),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: buildCategory(
                          icon:FontAwesomeIcons.calendarCheck,
                          label:"客户管理",
                          color:Colors.deepOrange.withOpacity(0.7),
                          onTap: (){
                            print("打开客户管理页面");
                          },
                        ),
                      ),
                      const SizedBox(width: 16.0),
                      Expanded(
                        child: buildCategory(
                            icon:FontAwesomeIcons.lock,
                            label:"联系人管理",
                            color: Colors.blue.withOpacity(0.6)
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 16.0),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: buildCategory(
                            icon: FontAwesomeIcons.bookmark,
                            label: "商机管理",
                            color: Colors.indigo.withOpacity(0.7)
                        ),
                      ),
                      const SizedBox(width: 16.0),
                      Expanded(
                        child: buildCategory(
                            icon: FontAwesomeIcons.file,
                            label: "投标管理",
                            color: Colors.greenAccent
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 16.0),
                ],

              ),
            ),
          )
      );
    }

    return SingleChildScrollView(
        child: Container(
          height: duSetHeight(800),
          child: Stack(
            children: [
              _buildBackgroundImage(),
              _buildCategoryApp(),
            ],
          ),
        )
    );
  }
}


