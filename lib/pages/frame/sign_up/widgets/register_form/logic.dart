import 'package:snowy_flutter/common/utils/validator.dart';
import 'package:snowy_flutter/common/widgets/toast.dart';
import 'package:get/get.dart';

import 'state.dart';

class RegisterFormLogic extends GetxController {
  final state = RegisterFormState();

  // 执行注册操作
  handleSignUp() {
    if (!duCheckStringLength(state.fullNameController.value.text, 5)) {
      toastInfo(msg: '用户名不能小于5位');
      return;
    }
    if (!duIsEmail(state.emailController.value.text)) {
      toastInfo(msg: '请正确输入邮件');
      return;
    }
    if (!duCheckStringLength(state.passController.value.text, 6)) {
      toastInfo(msg: '密码不能小于6位');
      return;
    }
    Get.back();
  }
}
