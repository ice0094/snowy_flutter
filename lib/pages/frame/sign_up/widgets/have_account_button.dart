import 'package:flutter/material.dart';
import 'package:snowy_flutter/common/utils/screen.dart';
import 'package:snowy_flutter/common/values/colors.dart';
import 'package:snowy_flutter/common/widgets/button.dart';
import 'package:get/get.dart';
class HaveAccountButton extends StatelessWidget {
  const HaveAccountButton({Key? key}) : super(key: key);
  // 返回上一页
  _handleNavPop() {
    Get.back();
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.only(bottom: duSetHeight(20)),
      child: btnFlatButtonWidget(
        onPressed: _handleNavPop,
        width: 294,
        gbColor: AppColors.secondaryElement,
        fontColor: AppColors.primaryText,
        title: "I have an account",
        fontWeight: FontWeight.w500,
        fontSize: 16,
      ),
    );
  }
}
