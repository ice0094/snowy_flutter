import 'package:flutter/material.dart';
import 'package:snowy_flutter/common/values/colors.dart';
import 'package:snowy_flutter/common/widgets/widgets.dart';
import 'package:snowy_flutter/common/widgets/toast.dart';
import 'package:snowy_flutter/common/widgets/app.dart';
import 'package:snowy_flutter/pages/frame/sign_up/widgets/have_account_button.dart';
import 'package:snowy_flutter/pages/frame/sign_up/widgets/register_form/view.dart';
import 'package:snowy_flutter/pages/frame/sign_up/widgets/third_party_login.dart';
import 'package:get/get.dart';

import 'logic.dart';
import 'state.dart';

class SignUpPage extends StatelessWidget {
  final SignUpLogic logic = Get.put(SignUpLogic());
  final SignUpState state = Get.find<SignUpLogic>().state;

  // logo
  Widget _buildLogo() {
    return Container();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: transparentAppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: AppColors.primaryText,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.info_outline,
              color: AppColors.primaryText,
            ),
            onPressed: () {
              toastInfo(msg: '这是注册界面');
            },
          )
        ],
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Divider(height: 1),
            _buildLogo(),
            RegisterForm(),
            Spacer(),
            ThirdPartyLogin(),
            HaveAccountButton(),
          ],
        ),
      ),
    );
  }
}
