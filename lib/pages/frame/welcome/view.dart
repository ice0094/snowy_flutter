import 'package:flutter/material.dart';
import 'package:snowy_flutter/common/routes/app_pages.dart';
import 'package:snowy_flutter/global.dart';
import 'package:snowy_flutter/pages/frame/welcome/widget/feature_item.dart';
import 'package:snowy_flutter/pages/frame/welcome/widget/header_detail.dart';
import 'package:snowy_flutter/pages/frame/welcome/widget/header_title.dart';
import 'package:snowy_flutter/pages/frame/welcome/widget/start_button.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'logic.dart';
import 'state.dart';

class WelcomePage extends StatelessWidget {
  final WelcomeLogic logic = Get.put(WelcomeLogic());
  final WelcomeState state = Get.find<WelcomeLogic>().state;

  @override
  Widget build(BuildContext context) {
    // 高度去掉 顶部、底部 导航
    ScreenUtil.init(
      BoxConstraints(
        maxWidth: 375,
        maxHeight: 812 - 44 - 34,
      ),
    );

    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            HeaderTitle(title: "欢迎页",),
            HeaderDetail(detail: "The best of news channels all in one place. Trusted sources and personalized news for you.",),
            FeatureItem(
              "feature-1",
              "Compelling photography and typography provide a beautiful reading",
              20,
            ),
            FeatureItem(
              "feature-2",
              "Sector news never shares your personal data with advertisers or publishers",
              20,
            ),
            FeatureItem(
              "feature-3",
              "You can get Premium to unlock hundreds of publications",
              20,
            ),
            Spacer(),
            StartButton(
              startButText: "开始",
              startButCallback: () {
                Global.saveAlreadyOpen();
                Get.toNamed(AppRoutes.SIGN_IN);
            },
            ),
          ],
        ),
      ),
    );
  }
}
