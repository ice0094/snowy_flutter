import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:snowy_flutter/common/utils/screen.dart';
import 'package:snowy_flutter/common/values/colors.dart';
import 'package:snowy_flutter/common/values/values.dart';

class StartButton extends StatelessWidget {

  final String startButText;

  final void Function() startButCallback;

  StartButton({Key? key, required this.startButText, required this.startButCallback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: duSetWidth(295),
      height: duSetHeight(44),
      margin: EdgeInsets.only(bottom: duSetHeight(20)),
      child: TextButton(
        style: ButtonStyle(
          textStyle: MaterialStateProperty.all(TextStyle(
            fontSize: 16,
          )),
          foregroundColor: MaterialStateProperty.resolveWith(
                (states) {
              if (states.contains(MaterialState.focused) &&
                  !states.contains(MaterialState.pressed)) {
                return Colors.blue;
              } else if (states.contains(MaterialState.pressed)) {
                return Colors.deepPurple;
              }
              return AppColors.primaryElementText;
            },
          ),
          backgroundColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.pressed)) {
              return Colors.blue[200];
            }
            return AppColors.primaryElement;
          }),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
            borderRadius: Radii.k6pxRadius,
          )),
        ),
        child: Text(startButText),
        onPressed: startButCallback,
      ),
    );
  }
}
