import 'package:flutter/cupertino.dart';
import 'package:snowy_flutter/common/utils/screen.dart';
import 'package:snowy_flutter/common/values/colors.dart';

/// 页头标题
class HeaderTitle extends StatelessWidget {

  final String title;

  HeaderTitle({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: duSetHeight(65)),
      child: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: AppColors.primaryText,
          fontFamily: "Montserrat",
          fontWeight: FontWeight.w600,
          fontSize: duSetFontSize(24),
        ),
      ),
    );
  }
}