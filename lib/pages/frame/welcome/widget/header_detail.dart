import 'package:flutter/cupertino.dart';
import 'package:snowy_flutter/common/utils/screen.dart';
import 'package:snowy_flutter/common/values/colors.dart';

class HeaderDetail extends StatelessWidget {

  final String detail;

  HeaderDetail({Key? key, required this.detail}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: duSetWidth(242),
      height: duSetHeight(70),
      margin: EdgeInsets.only(top: duSetHeight(14)),
      child: Text(
        detail,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: AppColors.primaryText,
          fontFamily: "Avenir",
          fontWeight: FontWeight.normal,
          fontSize: duSetFontSize(16),
          height: 1.3,
        ),
      ),
    );
  }
}