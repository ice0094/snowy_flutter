import 'package:flutter/material.dart';
import 'package:snowy_flutter/common/utils/screen.dart';
import 'package:snowy_flutter/common/widgets/captcha/captcha.dart';
import 'package:snowy_flutter/common/widgets/image.dart';
import 'package:snowy_flutter/pages/frame/sign_in/widgets/login_form.dart';
import 'package:snowy_flutter/pages/frame/sign_in/widgets/logo.dart';
import 'package:get/get.dart';

import 'logic.dart';
import 'state.dart';

class SignInPage extends StatelessWidget {
  final SignInLogic logic = Get.put(SignInLogic());
  final SignInState state = Get.find<SignInLogic>().state;


  // // 第三方登录
  // Widget _buildThirdPartyLogin() {
  //   return Container();
  // }
  //
  // // 注册按钮
  // Widget _buildSignupButton() {
  //   return Container();
  // }

  @override
  Widget build(BuildContext context) {


    return  Scaffold(
      resizeToAvoidBottomInset: false,
      body: Center(
        child: Column(
          children: <Widget>[
            Logo(
              logoUrl: "assets/images/logo.png",
              mainTitle: "XIAONUO",
              subTitle: "albed",
            ),
            LoginForm(
              accountController: state.accountController.value,
              passController: state.passController.value,
              handleNavSignUp: logic.handleNavSignUp,
              confirm: (){
                logic.confirm(context);
              },
            ),
            // ElevatedButton(onPressed: (){
            //   loadingBlockPuzzle(context);
            // }, child: Text("拼图校验")),
            // ElevatedButton(onPressed: (){
            //   loadingClickWord(context);
            // }, child: Text("文字校验")),
            Spacer(),
            // _buildThirdPartyLogin(),
            // _buildSignupButton(),
          ],
        ),
      ),
    );
  }
}
