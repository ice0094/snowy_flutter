import 'package:flutter/cupertino.dart';
import 'package:snowy_flutter/common/values/colors.dart';
import 'package:snowy_flutter/common/utils/screen.dart';
import 'package:snowy_flutter/common/values/values.dart';

class Logo extends StatelessWidget {

  final String logoUrl;
  final String? mainTitle;
  final String? subTitle;

  Logo({Key? key, required this.logoUrl, this.mainTitle, this.subTitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: duSetWidth(110),
      margin: EdgeInsets.only(top: duSetHeight(40 + 44.0)), // 顶部系统栏 44px
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            height: duSetHeight(80),
            width: duSetWidth(80),
            margin: EdgeInsets.symmetric(horizontal: duSetWidth(15)),
            child: Stack(
              alignment: Alignment.center,
              children: [
                Positioned(
                  left: 0,
                  top: 0,
                  right: 0,
                  child: Container(
                    height: duSetWidth(76),
                    decoration: BoxDecoration(
                      color: AppColors.primaryBackground,
                      boxShadow: [
                        Shadows.primaryShadow,
                      ],
                      borderRadius: BorderRadius.all(
                          Radius.circular(duSetWidth(76 * 0.5))), // 父容器的50%
                    ),
                    child: Container(),
                  ),
                ),
                Positioned(
                  top: duSetWidth(8),
                  child: Image.asset(
                    logoUrl,
                    fit: BoxFit.fill,
                    width: 65.0,
                    height: 65.0,
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: duSetHeight(10)),
            child: Text(
              mainTitle??"",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: AppColors.primaryText,
                fontFamily: "Montserrat",
                fontWeight: FontWeight.w500,
                fontSize: duSetFontSize(22),
                height: 1,
              ),
            ),
          ),
          Text(
            subTitle??"",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: AppColors.primaryText,
              fontFamily: "Avenir",
              fontWeight: FontWeight.w400,
              fontSize: duSetFontSize(16),
              height: 1,
            ),
          ),
        ],
      ),
    );;
  }
}
