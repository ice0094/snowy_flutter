import 'package:flutter/material.dart';
import 'package:snowy_flutter/common/utils/screen.dart';
import 'package:snowy_flutter/common/values/colors.dart';
import 'package:snowy_flutter/common/widgets/widgets.dart';

class LoginForm extends StatelessWidget {
  final TextEditingController? accountController;
  final TextEditingController? passController;
  final VoidCallback? handleNavSignUp;
  final VoidCallback? confirm;

  const LoginForm({Key? key, this.accountController, this.passController, this.handleNavSignUp, this.confirm}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      width: duSetWidth(295),
      // height: 204,
      margin: EdgeInsets.only(top: duSetHeight(49)),
      child: Column(
        children: [

          // account input
          inputTextEdit(
            controller: accountController,
            keyboardType: TextInputType.text,
            hintText: "请输入账户",
            marginTop: 0,
          ),
          // password input
          inputTextEdit(
            controller: passController,
            keyboardType: TextInputType.visiblePassword,
            hintText: "请输入密码",
            isPassword: true,
          ),
          // 注册、登录 横向布局
          Container(
            height: duSetHeight(44),
            margin: EdgeInsets.only(top: duSetHeight(15)),
            child: Row(
              children: [
                // 注册
                btnFlatButtonWidget(
                  onPressed: handleNavSignUp,
                  gbColor: AppColors.thirdElement,
                  title: "注册",
                ),
                Spacer(),
                // 登录
                btnFlatButtonWidget(
                  onPressed: confirm,
                  gbColor: AppColors.primaryElement,
                  title: "确认",
                ),
              ],
            ),
          ),
          // Spacer(),

          // Fogot password
          Container(
            height: duSetHeight(22),
            margin: EdgeInsets.only(top: duSetHeight(20)),
            child: FlatButton(
              onPressed: () => {},
              child: Text(
                "Fogot password?",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: AppColors.secondaryElementText,
                  fontFamily: "Avenir",
                  fontWeight: FontWeight.w400,
                  fontSize: duSetFontSize(16),
                  height: 1, // 设置下行高，否则字体下沉
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
