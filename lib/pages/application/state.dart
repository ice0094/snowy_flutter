import 'package:snowy_flutter/common/values/values.dart';
import 'package:snowy_flutter/pages/account/view.dart';
import 'package:snowy_flutter/pages/main/view.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class ApplicationState {
  List tabPageConfigList = [
    {
      "name" : "主页",
      "page" : MainPage(),
      "item": BottomNavigationBarItem(
        label: "首页",
        icon: Icon(
          Icons.home,
          color: AppColors.tabBarElement,
        ),
        activeIcon: Icon(
          Icons.home,
          color: AppColors.secondaryElementText,
        ),
      )
    },
    {
      "name" : "我的",
      "page" : AccountPage(),
      "item": BottomNavigationBarItem(
        label: "我的",
        icon: Icon(
          Icons.account_circle,
          color: AppColors.tabBarElement,
        ),
        activeIcon: Icon(
          Icons.account_circle,
          color: AppColors.secondaryElementText,
        ),
      )
    }
  ];

  // 变量声明定义
  RxInt curIndex = 0.obs;
  RxList<String> tapPageNameRxList = <String>[].obs;
  RxList<Widget> tapPageRxList = <Widget>[].obs;
  RxList<BottomNavigationBarItem> bottomNavigationBarItemRxList = <BottomNavigationBarItem>[].obs;

  // 页控制器
  late final PageController pageController;

  ApplicationState() {
    tabPageConfigList.forEach((item) {
      tapPageNameRxList.add(item['name']);
      tapPageRxList.add(item['page']);
      bottomNavigationBarItemRxList.add(item['item']);
    });
  }
}
