import 'package:flutter/material.dart';
import 'package:snowy_flutter/common/utils/utils.dart';
import 'package:snowy_flutter/common/values/values.dart';
import 'package:snowy_flutter/common/widgets/app.dart';
import 'package:get/get.dart';

import 'logic.dart';
import 'state.dart';

class ApplicationPage extends GetView<ApplicationLogic> {
  final ApplicationLogic logic = Get.put(ApplicationLogic());
  final ApplicationState state = Get.find<ApplicationLogic>().state;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: transparentAppBar(
          title: Obx(()=> Text(
            state.tapPageNameRxList[controller.state.curIndex.value],
            style: TextStyle(
              color: AppColors.primaryText,
              fontFamily: 'Montserrat',
              fontSize: duSetFontSize(18.0),
              fontWeight: FontWeight.w600,
            ),
          )),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: AppColors.primaryText,
            ),
            onPressed: () {
              Get.back();
            },
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.search,
                color: AppColors.primaryText,
              ),
              onPressed: () {},
            )
          ]
        ),
        // 这个方法也是可以的
        // body: Obx(()=>state.tapPageRxList[state.curIndex.value]),
        body:PageView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            ...state.tapPageRxList
          ],
          controller: state.pageController,
          onPageChanged: (int page) {
            state.curIndex.value = page;
          },
        ),
        bottomNavigationBar: Obx(() => BottomNavigationBar(
        // 当前页面
        currentIndex: state.curIndex.value,
        // 底部菜单栏设置
        items: state.bottomNavigationBarItemRxList,
        // fixedColor: AppColors.primaryElement,
        // 配置底部菜单有多个按钮（当底部菜单栏溢出的时候可使用该功能）
        type: BottomNavigationBarType.fixed,
        // 点击触发的事件
        onTap: logic.chgCurIndex,
        showSelectedLabels: false,
        showUnselectedLabels: false,
      ))
    );
  }
}
