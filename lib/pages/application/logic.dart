import 'package:snowy_flutter/pages/main/logic.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'state.dart';

class ApplicationLogic extends GetxController {

  ApplicationLogic();


  final state = ApplicationState();





  @override
  void onInit() {
    super.onInit();
    state.pageController = new PageController(initialPage: state.curIndex.value);
  }

  void chgCurIndex(int index){
    // state.curIndex.value = index;
    state.pageController.animateToPage(index, duration: const Duration(milliseconds: 200), curve: Curves.ease);
  }


  @override
  void dispose() {
    print('ApplicationLogic被销毁了');
    // pageController.dispose();
    super.dispose();
  }

}
