import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
// import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';

import 'init/init.dart';
import 'common/routes/app_pages.dart';

void main() async{
  await Init.run();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(420,812-44-34),
      builder: ()=> GetMaterialApp(
        // // flutter 国际化配置 start
        // localizationsDelegates: [
        //   GlobalMaterialLocalizations.delegate,
        //   GlobalWidgetsLocalizations.delegate,
        // ],
        // supportedLocales: [
        //   const Locale("zh","CH"),
        //   const Locale("en", "US"),
        // ],
        // // flutter 国际化配置 end
        // 去掉右边上角的debugBanner
        debugShowCheckedModeBanner: false,
        initialRoute: AppPages.INITIAL,
        getPages: AppPages.routes,
        // unknownRoute: AppPages.unknownRoute,
        enableLog: true,
        builder: EasyLoading.init(),
        // routingCallback: _routingCallback,
      ),
    );
  }
}
