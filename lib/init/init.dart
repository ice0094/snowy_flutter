
//应用初始化
import 'package:flutter/cupertino.dart';
import 'package:snowy_flutter/common/utils/http/http.dart';
import 'package:snowy_flutter/common/utils/storage.dart';

import '../global.dart';

class Init {
  static Future<void> run() async{
    // 运行初始
    WidgetsFlutterBinding.ensureInitialized();
    // SharedPreferences 初始化
    await StorageUtil.init();
    // 全局数据初始化
    Global.init();
    // http 初始化
    HttpUtil();


  }
}

