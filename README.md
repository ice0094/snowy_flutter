
# 项目准备

拉取小诺项目，并启动小诺后端项目（关于小诺的后端项目相关操作，再次不进行赘述，可去小诺的官方进行了解：https://xiaonuo.vip/）

![输入图片说明](https://images.gitee.com/uploads/images/2021/0706/214728_8831f971_855475.png "屏幕截图.png")

# 开始使用

## 下载项目

https://gitee.com/icefox1/flutter_app.git

## 拉去依赖

![输入图片说明](https://images.gitee.com/uploads/images/2021/0706/214738_f41498ba_855475.png "屏幕截图.png")



## 配置后端的访问地址

![输入图片说明](https://images.gitee.com/uploads/images/2021/0706/214754_9ba78bb8_855475.png "屏幕截图.png")



## web容器运行

![输入图片说明](https://images.gitee.com/uploads/images/2021/0706/214811_5c20b973_855475.png "屏幕截图.png")

## 运行效果

### 1、欢迎页

![输入图片说明](https://images.gitee.com/uploads/images/2021/0706/214817_b64ed4e9_855475.png "屏幕截图.png")

### 2、登录页

![输入图片说明](https://images.gitee.com/uploads/images/2021/0706/214825_a4ac71d8_855475.png "屏幕截图.png")

### 3、验证码

![输入图片说明](https://images.gitee.com/uploads/images/2021/0706/214830_1380f63d_855475.png "屏幕截图.png")

时候启用验证码是小诺的后端决定的

### 4、主页

![输入图片说明](https://images.gitee.com/uploads/images/2021/0706/214835_f4ce330e_855475.png "屏幕截图.png")

### 5、个人中心

![输入图片说明](https://images.gitee.com/uploads/images/2021/0706/214843_f4300c35_855475.png "屏幕截图.png")



安卓的演示效果于web端基本一直，可自行选择相应的容器进行演示，ios端作者暂未进行进行测试



## lib目录结构

```bash
├── common					 # 抽离文件
│   └── apis                 # 接口
|   └── dto                  # dto（服务接口的对象）
|   └── middlewares 		 # getX中间件（用于鉴权、路由重定向）
|   └── model 		         # model对象（除接口的对象，其他对象都放在此处）
|   └── routes 		         # 路由管理
|   └── utils 		         # 工具类
|   └── values 		         # 常量
|   └── widget 		         # 公共组件
├── init					 # 初始化文件
├── pages					 # 页面
```

## 



# web打包注意事项

- 1、web 打包命令 flutter build web --web-renderer html
- 2、web/index.html 要修改两个地方 14行和 67行

   注释掉14行
   67行修改成（兼容安卓和微信移动端）：
   `if (reg.installing == null) waitForActivation(reg.waiting)    // << Modified here
   else waitForActivation(reg.installing);                                 // << Modified here
   //waitForActivation(reg.installing ?? reg.waiting);              // << Modified here`

- 3、Uncaught TypeError: Cannot read property 'toString' of null 该错误未修复